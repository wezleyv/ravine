
#include "custommotor.h"

CustomMotor::CustomMotor()
{
}

CustomMotor::~CustomMotor()
{
}

void
CustomMotor::Configure(int pwmPinA, int pwmPinB)
{
  m_pwmPinA = pwmPinA;
  m_pwmPinB = pwmPinB;
  pinMode(m_pwmPinA, OUTPUT);
  pinMode(m_pwmPinB, OUTPUT);
}

void
CustomMotor::SetDirection(int dir)
{
    if ( m_direction != dir ) {
      m_direction = dir;
    }
}

void
CustomMotor::SetSpeed(char motorSpeed)
{
    if ( m_speed != motorSpeed ) {
      m_speed = motorSpeed;
    }
}

void
CustomMotor::Activate()
{
    if ( m_direction == FORWARD ) {
      analogWrite(m_pwmPinA, m_speed);
      analogWrite(m_pwmPinB, 0);
    }
    else {
      analogWrite(m_pwmPinB, m_speed);
      analogWrite(m_pwmPinA, 0);
    }
}

void
CustomMotor::Deactivate()
{
    analogWrite(m_pwmPinA, 0);
    analogWrite(m_pwmPinB, 0);
}
