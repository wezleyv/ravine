#include "stdafx.h"
#include "custommotor.h"

class ScissorLift
{
  private:
  CustomMotor   m_liftMotor;

  public:
  ScissorLift(int, int);
  ~ScissorLift();

  public:
  void Extend(int);
  void Retract(int);
};
