#include "stdafx.h"
#include "motor.h"
#include "sensor.h"

class DriveTrain
{
  private:
  int     m_lws;
  int     m_rws;
  bool    m_lwd;
  bool    m_rwd;
  bool    m_stLdone;
  bool    m_stRdone;
  unsigned int m_lastTime;
  Motor   m_leftMotor;
  Motor   m_rightMotor;

  public:
  DriveTrain(int, int, int, int);
  ~DriveTrain();

  public:
  void TurnLeft();
  void TurnRight();
  void DriveForward();
  void Stop();
  void Brake();
  void UseFeedback(Sensor, Sensor);
  void OvercomeST();
};
