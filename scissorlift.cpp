
#include "scissorlift.h"

ScissorLift::ScissorLift(int motorPinA, int motorPinB)
{
  m_liftMotor.Configure(motorPinA, motorPinB);
}

ScissorLift::~ScissorLift()
{
}

void
ScissorLift::Extend(int timeSpan)
{
  m_liftMotor.SetDirection(REVERSE);
  m_liftMotor.SetSpeed(255);
  m_liftMotor.Activate();
  delay(timeSpan);
  m_liftMotor.Deactivate();
}

void
ScissorLift::Retract(int timeSpan)
{
  m_liftMotor.SetDirection(FORWARD);
  m_liftMotor.SetSpeed(255);
  m_liftMotor.Activate();
  delay(timeSpan);
  m_liftMotor.Deactivate();
}
