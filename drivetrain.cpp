
#include "stdafx.h"
#include "drivetrain.h"
#include "sensor.h"

DriveTrain::DriveTrain(int lmsPin, int rmsPin, int lmdPin, int rmdPin)
{
  m_leftMotor.Configure(lmsPin, lmdPin);
  m_rightMotor.Configure(rmsPin, rmdPin);
  m_stLdone = false;
  m_stRdone = false;
  m_lastTime = 0;
}

DriveTrain::~DriveTrain()
{
}

void
DriveTrain::DriveForward()
{
  m_lwd = FORWARD;
  m_rwd = FORWARD;
  m_leftMotor.SetDirection(m_lwd);
  m_rightMotor.SetDirection(m_rwd);
  OvercomeST();
  m_leftMotor.SetSpeed(BASE_SPEED);
  m_rightMotor.SetSpeed(BASE_SPEED);
  m_leftMotor.Activate();
  m_rightMotor.Activate();
}

void
DriveTrain::TurnLeft()
{
  m_lwd = REVERSE;
  m_rwd = FORWARD;
  m_leftMotor.SetDirection(m_lwd);
  m_rightMotor.SetDirection(m_rwd);
  OvercomeST();
  m_leftMotor.SetSpeed(BASE_SPEED);
  m_rightMotor.SetSpeed(BASE_SPEED - 60);
  m_leftMotor.Activate();
  m_rightMotor.Activate();
}

void
DriveTrain::TurnRight()
{
  m_lwd = FORWARD;
  m_rwd = REVERSE;
  m_leftMotor.SetDirection(m_lwd);
  m_rightMotor.SetDirection(m_rwd);
  OvercomeST();
  m_leftMotor.SetSpeed(BASE_SPEED);
  m_rightMotor.SetSpeed(BASE_SPEED);
  m_leftMotor.Activate();
  m_rightMotor.Activate();
  delay(750);
  Stop();
}

void
DriveTrain::Brake()
{
  m_leftMotor.SetDirection(!m_lwd);
  m_rightMotor.SetDirection(!m_rwd);
  m_leftMotor.SetSpeed(255);
  m_rightMotor.SetSpeed(255);
  m_leftMotor.Activate();
  m_rightMotor.Activate();
  delay(80);
}

void
DriveTrain::Stop()
{
  Brake();
  m_leftMotor.Deactivate();
  m_rightMotor.Deactivate();
  m_stLdone = false;
  m_stRdone = false;
}

void
DriveTrain::UseFeedback(Sensor onTableFeedbackSensor, Sensor offTableFeedbackSensor)
{
  int dt = millis() - m_lastTime;
  int error = dt * 1;

  if ( error + BASE_SPEED > 255 ) {
    error = 255 - BASE_SPEED;
  }
  
  // ALIGNED, DRIVE STRAIGHT
  if ( onTableFeedbackSensor.IsAbove(TABLE_FEEDBACK_THRESH) && offTableFeedbackSensor.IsBelow(TABLE_FEEDBACK_THRESH) ) {
      m_lwd = FORWARD;
      m_rwd = FORWARD;
      m_leftMotor.SetDirection(m_lwd);
      m_rightMotor.SetDirection(m_rwd);
      m_leftMotor.SetSpeed(BASE_SPEED);
      m_rightMotor.SetSpeed(BASE_SPEED);
      m_leftMotor.Activate();
      m_rightMotor.Activate();
      m_lastTime = millis();
  }

  // TOO FAR LEFT, DRIVE RIGHT (BOTH SENSORS ON TABLE)
  if ( onTableFeedbackSensor.IsAbove(TABLE_FEEDBACK_THRESH) && offTableFeedbackSensor.IsAbove(TABLE_FEEDBACK_THRESH) ) {
      m_lwd = FORWARD;
      m_rwd = FORWARD;
      m_leftMotor.SetDirection(m_lwd);
      m_rightMotor.SetDirection(m_rwd);
      m_leftMotor.SetSpeed(BASE_SPEED - error);
      m_rightMotor.SetSpeed(BASE_SPEED + error);
      m_leftMotor.Activate();
      m_rightMotor.Activate();
  }

  // TOO FAR RIGHT, DRIVE LEFT (BOTH SENSORS OFF TABLE)
  if ( onTableFeedbackSensor.IsBelow(TABLE_FEEDBACK_THRESH) && offTableFeedbackSensor.IsBelow(TABLE_FEEDBACK_THRESH) ) {
    m_lwd = FORWARD;
    m_rwd = FORWARD;
    m_leftMotor.SetDirection(m_lwd);
    m_rightMotor.SetDirection(m_rwd);
    m_leftMotor.SetSpeed(BASE_SPEED + error);
    m_rightMotor.SetSpeed(BASE_SPEED - error);
    m_leftMotor.Activate();
    m_rightMotor.Activate();
  }
}

void DriveTrain::OvercomeST()
{
  if ( !m_stLdone && !m_stRdone ) {
    m_leftMotor.SetSpeed(255);
    m_rightMotor.SetSpeed(255);
    m_leftMotor.Activate();
    m_rightMotor.Activate();
    m_stLdone = true;
    m_stRdone = true;
  }
  else if ( !m_stLdone ) {
    m_leftMotor.SetSpeed(255);
    m_leftMotor.Activate();
    m_stLdone = true;
  }
  else if ( !m_stRdone ) {
    m_rightMotor.SetSpeed(255);
    m_rightMotor.Activate();
    m_stRdone = true;
  }
  delay(200);
}


