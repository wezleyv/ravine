#ifndef MOTOR_F1DE933C_C7E7_4F02_8EEE_B44969E41F63
#define MOTOR_F1DE933C_C7E7_4F02_8EEE_B44969E41F63

#include "stdafx.h"

class CustomMotor
{
private:
  int   m_pwmPinA;
  int   m_pwmPinB;
  char  m_speed;
  int   m_direction;

public:
  CustomMotor();
  ~CustomMotor();

public:
  void Configure(int, int);
  void SetDirection(int);
  void SetSpeed(char);
  void Activate();
  void Deactivate();
};

#endif // MOTOR_F1DE933C_C7E7_4F02_8EEE_B44969E41F63

