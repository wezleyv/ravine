
#include "stdafx.h"
#include "grapple.h"

Grapple::Grapple(int motorPinA, int motorPinB, int servoPinL, int servoPinR)
{
  m_traversalMotors.Configure(motorPinA, motorPinB);
}

Grapple::~Grapple()
{
}

void
Grapple::AttachServos(int servoPinL, int servoPinR)
{
  m_clampServoL.attach(servoPinL);
  m_clampServoR.attach(servoPinR);
  m_clampServoL.write(30);
  m_clampServoR.write(30);
}

void
Grapple::Clamp(int pos)
{
  m_clampServoL.write(pos - 10);
  m_clampServoR.write(pos);
}

void
Grapple::Unclamp()
{
  m_clampServoL.write(30);
  m_clampServoR.write(30);
}

void
Grapple::Traverse()
{
  m_traversalMotors.SetDirection(REVERSE);
  m_traversalMotors.SetSpeed(255);
  m_traversalMotors.Activate();
}

void
Grapple::Stop()
{
  m_traversalMotors.Deactivate();
}
