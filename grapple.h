#include <Servo.h>
#include "stdafx.h"
#include "custommotor.h"

class Grapple
{
  private:
  Servo         m_clampServoL;
  Servo         m_clampServoR;
  CustomMotor   m_traversalMotors;

  public:
  Grapple(int, int, int, int);
  ~Grapple();

  public:
  void AttachServos(int, int);
  void Clamp(int);
  void Unclamp();
  void Traverse();
  void Stop();
};
