#include "stdafx.h"
#include "drivetrain.h"
#include "sensor.h"
#include "scissorlift.h"
#include <Servo.h>
#include "grapple.h"

Sensor frontEdgeSensor(2, ANALOG);
Sensor onTableFeedbackSensor(5, ANALOG);
Sensor offTableFeedbackSensor(6, ANALOG);
Sensor poleSupportSensor(7, ANALOG);
Sensor poleFaceSensor(3, ANALOG);
DriveTrain drivetrain(11, 3, 13, 12);   // Use motor sheild for drivetrain
ScissorLift scissorlift(10, 7);   // Use driver chip for scissor lift motor (multidirectional)
Grapple grapple(2, 4, 22, 23);    // Use motor sheild for clamp servo control, use driver chip for traversal motor (omnidirectional)

bool done = 0;

void setup() {
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(22, OUTPUT);
  pinMode(23, OUTPUT);
  Serial.begin(9600);
  grapple.AttachServos(22, 23);
}

void loop() {
  if ( !done ) {
    Serial.println("Waiting to start...");
    delay(5000);
    Execute();
    //Test();
  }
}

void Test()
{
  Serial.println("Driving out of start point...");
  while ( frontEdgeSensor.IsAbove(TABLE_EDGE_THRESH) ) {
    drivetrain.DriveForward();
  }
  drivetrain.Stop();
  delay(300);

  Serial.println("Reached edge of table, turning left...");
  while ( offTableFeedbackSensor.IsAbove(TABLE_FEEDBACK_THRESH) ) {
    drivetrain.TurnLeft();
  }
  drivetrain.Stop();
  delay(300);
  done = 1;
}

bool Execute()
{
  Serial.println("Driving out of start point...");
  while ( frontEdgeSensor.IsAbove(TABLE_EDGE_THRESH) ) {
    drivetrain.DriveForward();
  }
  drivetrain.Stop();
  delay(100);

  Serial.println("Reached edge of table, turning left...");
  while ( offTableFeedbackSensor.IsAbove(TABLE_FEEDBACK_THRESH) ) {
    drivetrain.TurnLeft();
  }
  drivetrain.Stop();
  delay(300);

  Serial.println("Driving to pole...");
  while ( poleSupportSensor.IsBelow(400) ) {
    drivetrain.UseFeedback(onTableFeedbackSensor, offTableFeedbackSensor);
  }
  drivetrain.Stop();
  
  Serial.println("Reached pole, extending scissor lift...");
  scissorlift.Extend(1000);
  grapple.Clamp(150);
  scissorlift.Retract(1000);
  grapple.Clamp(160);
  
  Serial.println("Clamped onto pole, traversing ravine...");
  while ( poleFaceSensor.IsBelow(100) ) {
    grapple.Traverse();
  }
  grapple.Stop();

  Serial.println("Reached other side of ravine, extending scissor lift...");
  scissorlift.Extend(2000);
  grapple.Unclamp();
  scissorlift.Retract(1000);

  Serial.println("Unclamped from pole, driving to table edge...");
  while ( frontEdgeSensor.IsAbove(TABLE_EDGE_THRESH) ) {
    drivetrain.DriveForward();
  }
  drivetrain.Stop();
  delay(100);

  Serial.println("Reached edge of table, turning right...");
  drivetrain.TurnRight();
  delay(100);

  Serial.println("Driving to end point...");
  while ( frontEdgeSensor.IsAbove(TABLE_EDGE_THRESH) ) {
    drivetrain.DriveForward();
  }
  drivetrain.Stop();

  Serial.println("All done.");
  done = 1;
}

