
#include "Motor.h"

Motor::Motor()
{
}

Motor::~Motor()
{
}

void
Motor::Configure(int pwmPin, int dirPin)
{
  m_pwmPin = pwmPin;
  m_dirPin = dirPin;
  pinMode(m_pwmPin, OUTPUT);
  pinMode(m_dirPin, OUTPUT);
}

void
Motor::SetDirection(int dir)
{
    if ( m_direction != dir ) {
      m_direction = dir;
    }
}

void
Motor::SetSpeed(char motorSpeed)
{
    if ( m_speed != motorSpeed ) {
      m_speed = motorSpeed;
    }
}

void
Motor::Activate()
{
    digitalWrite(m_dirPin, m_direction);
    analogWrite(m_pwmPin, m_speed);
}

void
Motor::Deactivate()
{
    analogWrite(m_pwmPin, 0);
}
