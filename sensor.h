#ifndef SENSOR_2BC680DF_0B2A_409B_A8B5_5DF5274DE91E
#define SENSOR_2BC680DF_0B2A_409B_A8B5_5DF5274DE91E



#include "stdafx.h"

class Sensor
{
  private:
  int   m_pin;
  int   m_value;
  int   m_type;

  public:
  Sensor(int, int);
  ~Sensor();

  public:
  int GetValue();
  int IsBelow(int);
  int IsAbove(int);
};

#endif // SENSOR_2BC680DF_0B2A_409B_A8B5_5DF5274DE91E
