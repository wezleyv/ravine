#ifndef MOTOR_F1DE933C_C7E7_4F02_8EEE_B44969E41F62
#define MOTOR_F1DE933C_C7E7_4F02_8EEE_B44969E41F62

#include "stdafx.h"

class Motor
{
private:
  int   m_pwmPin;
  int   m_dirPin;
  char  m_speed;
  int   m_direction;

public:
  Motor();
  ~Motor();

public:
  void Configure(int, int);
  void SetDirection(int);
  void SetSpeed(char);
  void Activate();
  void Deactivate();
};

#endif // MOTOR_F1DE933C_C7E7_4F02_8EEE_B44969E41F62

