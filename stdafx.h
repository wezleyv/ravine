#ifndef STDAFX_8DD326A2_CCDB_47D4_BE06_89F8198EEEA3
#define STDAFX_8DD326A2_CCDB_47D4_BE06_89F8198EEEA3

#include <Arduino.h>

static const unsigned    ANALOG = 1;
static const unsigned    DIGITAL = 2;

static const unsigned    FORWARD = 0;
static const unsigned    REVERSE = 1;
static const unsigned    BASE_SPEED = 200;

static const unsigned    TABLE_EDGE_THRESH = 100;
static const unsigned    TABLE_FEEDBACK_THRESH = 160;

#endif // STDAFX_8DD326A2_CCDB_47D4_BE06_89F8198EEEA3
