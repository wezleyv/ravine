#include "sensor.h"

Sensor::Sensor(int pin, int type)
{
  m_pin = pin;
  m_type = type;

  if ( m_type == DIGITAL ) {
    pinMode(m_pin, INPUT);
  }
}

Sensor::~Sensor()
{
}

int
Sensor::GetValue()
{
  if ( m_type == ANALOG ) {
    m_value = analogRead(m_pin);
  }
  else if ( m_type == DIGITAL ) {
    m_value = digitalRead(m_pin);
  }
  return m_value;
}

int
Sensor::IsBelow(int threshold)
{
  if ( m_type == ANALOG ) {
    m_value = analogRead(m_pin);
    if ( m_value < threshold ) {
      return 1;
    }
  }
  return 0;
}

int
Sensor::IsAbove(int threshold)
{
  if ( m_type == ANALOG ) {
    m_value = analogRead(m_pin);
    if ( m_value > threshold ) {
      return 1;
    }
  }
  return 0;
}
